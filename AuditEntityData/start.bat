@echo off
set arg1=%1
shift

AUDITENTITYDATA.exe 0 %arg1%
IF NOT %ERRORLEVEL% == 0 goto somethingbad
exit

:somethingbad
AUDITENTITYDATA.exe %ERRORLEVEL% %arg1%
IF NOT %ERRORLEVEL% == 0 goto somethingbad
exit