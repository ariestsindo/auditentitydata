﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Text;

namespace MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP
{
    public partial class Program
    {
        internal static int IsErrorOccurred { get; set; }
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        [STAThread]
        static int Main(string[] args)
        {
            int rowNumber = 1;
            int rowNumber_error = 0;
            string hours = "1";
            bool testMode = false;

            if (args.Length == 2 && args[0] != "testrun")
            { 
                Logger.Info("Program started.");

                hours = args[1]; 
            }
            else if (args[0] == "testrun" && !string.IsNullOrEmpty(args[1]))
            { 
                Logger.Info("Program started in test mode.");

                testMode = true; 
                hours = args[1];
            }
            else
            { 
                rowNumber_error = Convert.ToInt32(args[0]);
                Logger.Warn("Program started from previous error.");
                Logger.Warn("Row (" + rowNumber_error + ") from previous error");

                hours = args[1];
            }

            Logger.Info($"Hour to retrieve: -{hours}");
            CrmServiceClient service = null;
            try
            {
                service = Helpers.Connect("Connect");
                if (!service.IsReady)
                {

                    if (string.IsNullOrEmpty(service.LastCrmError) == false)
                    {
                        Logger.Error(service.LastCrmError, "Error occurred.");
                        IsErrorOccurred = 1;
                        throw new Exception(service.LastCrmError);
                    }
                }

                Logger.Info("Connection is ready.");

                List<string> auditenabledentity = new List<string>();
                auditenabledentity = GetAuditEnabledEntity();
                Logger.Info("Total Audit Enabled Entity = " + auditenabledentity.Count);

                foreach (var _entity in auditenabledentity)
                    Logger.Info("Audit Enabled Entity = " + _entity);

                if (testMode)
                    Logger.Info("TEST MODE: No event logs will be written");

                foreach (var _entity in auditenabledentity)
                {
                    //  modifiedby: MLIJKT01\crmadminid, guid fix 981f9e71-3a55-e911-8139-0050568b72b2 semua envi
                    string fetchxml = 
                        @"<?xml version='1.0'?>
                            <fetch distinct='false' mapping='logical' no-lock='true'>
                                <entity name='" + _entity + @"'> 
                                    <filter type='and'> 
                                        <condition attribute='modifiedon' operator='last-x-hours' value='" + hours + @"' />
                                        <condition attribute='modifiedby' operator='neq' value='981f9e71-3a55-e911-8139-0050568b72b2' />
                                    </filter> 
                                    <order attribute='modifiedon' descending='true' /> 
                                </entity>
                            </fetch>";

                    ////EntityCollection records = service.RetrieveMultiple(new FetchExpression(fetchxml));

                    var result = new EntityCollection();
                    var pageNumber = 1;
                    var isMoreRecordExist = false;
                    SetFetchPagingQuery.SetPagingQuery(service, 500, new StringBuilder(fetchxml),
                        out result, pageNumber, out isMoreRecordExist);

                    Logger.Info("Total records of (" + _entity + ") with modifiedon last-" + hours + "-hours = " + result.Entities.Count +
                                ", pageNumber = " + pageNumber + ", isMoreRecordExist = " + isMoreRecordExist.ToString());

                    foreach (Entity _result in result.Entities)
                    {
                        GenerateEntityAuditHistory(service, _result.Id, _entity, ref rowNumber, ref rowNumber_error, testMode);
                    }

                    while (isMoreRecordExist)
                    {
                        result = null;
                        GC.Collect();
                        pageNumber++;
                        SetFetchPagingQuery.SetPagingQuery(service, 500, new StringBuilder(fetchxml),
                            out result, pageNumber, out isMoreRecordExist);

                        Logger.Info("Total records of (" + _entity + ") with modifiedon last-" + hours + "-hours = " + result.Entities.Count +
                                    ", pageNumber = " + pageNumber + ", isMoreRecordExist = " + isMoreRecordExist.ToString());
                        foreach (Entity _result in result.Entities)
                        {
                            GenerateEntityAuditHistory(service, _result.Id, _entity, ref rowNumber, ref rowNumber_error, testMode);
                        }
                    }
                }

                IsErrorOccurred = 0;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error occurred : " + ex.Message + "\nStack: " + ex.StackTrace);
                IsErrorOccurred = 1;
                Helpers.HandleException(ex);
            }

            finally
            {
                if (service != null)
                    service.Dispose();

                if (testMode && IsErrorOccurred == 0)
                {
                    Logger.Info("TEST MODE: Exited successfully.");
                }
                else if (IsErrorOccurred > 0)
                {
                    Logger.Error("Program exited with error.");
                }
                else
                {
                    Logger.Info("Program exited without error.");
                }
            }

            if (IsErrorOccurred > 0) {
                return rowNumber; 
            }
            else 
                return IsErrorOccurred;  
        }
    }
}
