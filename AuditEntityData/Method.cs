﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using NLog;
using System.Diagnostics;
using MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP.Encryption;
using System.IO;
using System.Configuration;

namespace MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP
{
    public partial class Program
    {  
        public class AuditTrails
        {
            public string auditId { get; set; }
            public AuditTrail auditTrail { get; set; }
        }

        public class AuditTrail
        {
            public string createdOn { get; set; }
            public string entity { get; set; }
            public string recordId { get; set; } 
            public string action { get; set; }
            public string operation { get; set; }
            public string performedBy { get; set; }
            public string performedById { get; set; }
            public Detail details { get; set; }
        }

        public class Detail
        {
            public string attributeChanged { get; set; }
            public string oldValue { get; set; }
            public string newValue { get; set; }
        } 

        private static int GenerateEntityAuditHistory(CrmServiceClient service, Guid recordID, string _entity, 
            ref int rowNumber, ref int rowNumber_error, bool testMode)
        { 
            var changeRequest = new RetrieveRecordChangeHistoryRequest
            {
                Target = new EntityReference(_entity, recordID)
            }; 

            var changeResponse = (RetrieveRecordChangeHistoryResponse)service.Execute(changeRequest);
            AuditDetailCollection details = changeResponse.AuditDetailCollection;

            foreach (AuditDetail detail in details.AuditDetails)
            { 
                var record = detail.AuditRecord;
                if (detail.GetType() == typeof(AttributeAuditDetail))
                {
                    var attributeDetail = (AttributeAuditDetail)detail;
                    if (attributeDetail.NewValue == null) continue;

                    ProcessAttributeDetail(record, _entity, ref rowNumber, ref rowNumber_error, detail, attributeDetail, testMode);
                }
            }

            return 0;
        }

        public static void ProcessAttributeDetail(Entity record, string _entity, ref int rowNumber, ref int rowNumber_error, 
            AuditDetail detail, AttributeAuditDetail attributeDetail, bool testMode)
        {
            string oldVal = "(no value)", newVal = "(no value)";
            foreach (var attribute in attributeDetail.NewValue.Attributes)
            {
                // email sudah di skip di SQL
                if (_entity == "incident" || _entity == "task") {
                    if ((AuditAction)(record["action"] as OptionSetValue).Value != AuditAction.Create)
                        continue;

                    if (!(attribute.Key == "createdon" || attribute.Key == "createdby"))
                        continue;
                }
                
                if (!record.Attributes.Contains("auditid"))
                {
                    Logger.Info(" " + rowNumber + ". [SKIP] AuditId is empty  (" + _entity + "), auditId = " + record["auditid"].ToString());
                    continue;
                }
                if (attributeDetail.OldValue.Contains(attribute.Key))
                {
                    oldVal = GetTypedValueAsString(attributeDetail.OldValue[attribute.Key]);
                    if (oldVal.Length > 500 || oldVal.Contains("\""))
                    {
                        oldVal = "complex value";
                    }
                }

                newVal = GetTypedValueAsString(attributeDetail.NewValue[attribute.Key]);
                if (!string.IsNullOrEmpty(newVal))
                {
                    if (newVal.Length > 500 || newVal.Contains("\""))
                    {
                        newVal = "complex value";
                    }
                }

                if (string.Equals(oldVal, newVal, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                var delimitedTabAuditTrail = string.Empty;
                delimitedTabAuditTrail = "\"" + record["auditid"].ToString() + "\",";
                delimitedTabAuditTrail += "\"" + (record["createdon"] as DateTime?).Value + "\",";
                delimitedTabAuditTrail += "\"" + _entity + "\",";
                delimitedTabAuditTrail += "\"" + (record["objectid"] as EntityReference).Id.ToString() + "\",";
                delimitedTabAuditTrail += "\"" + ((AuditAction)(record["action"] as OptionSetValue).Value).ToString() + "\",";
                delimitedTabAuditTrail += "\"" + ((AuditOperation)(record["operation"] as OptionSetValue).Value).ToString() + "\",";
                delimitedTabAuditTrail += "\"" + (record["userid"] as EntityReference).Name + "\",";
                delimitedTabAuditTrail += "\"" + (record["userid"] as EntityReference).Id.ToString() + "\",";
                delimitedTabAuditTrail += "\"" + attribute.Key + "\",";
                delimitedTabAuditTrail += "\"" + oldVal + "\",";
                delimitedTabAuditTrail += "\"" + newVal + "\"";

                if (rowNumber < rowNumber_error)
                {
                    Logger.Debug(" " + rowNumber + ") [SKIP] audit entity (" + _entity + "), auditId = "
                        + ((Guid)record["auditid"]).ToString() + ", recordId = " + (record["objectid"] as EntityReference).Id.ToString());
                    Logger.Debug("     Already written to event logs : " + delimitedTabAuditTrail);

                    rowNumber++;

                    continue;
                }

                if (!testMode)
                    WriteToEventLog(record, _entity, ref rowNumber, delimitedTabAuditTrail);
            }
        }

        public static void WriteToEventLog(Entity record, string _entity, ref int rowNumber, string delimitedTabAuditTrail)
        {
            Logger.Debug(" " + rowNumber + ") Retrieving audit entity (" + _entity + "), auditId = "
                            + ((Guid)record["auditid"]).ToString() + ", recordId = " + (record["objectid"] as EntityReference).Id.ToString());

            Logger.Debug("     Writing to event logs : " + delimitedTabAuditTrail);

#if DEBUG
            Logger.Debug("     Debug build: redirecting to .NET Runtime");
            EventLog.WriteEntry(".NET Runtime", delimitedTabAuditTrail, EventLogEntryType.Information, 1000);
#else
                        Logger.Debug("     Release build: using proper application namespace");
                        if (!EventLog.SourceExists("MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP"))
                        {
                            EventLog.CreateEventSource("MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP", "Application");
                        } 
                        EventLog.WriteEntry("MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP", delimitedTabAuditTrail, EventLogEntryType.Information, 1000);
#endif
            Logger.Debug("     Success writing to event logs.");

            rowNumber++; 
        }
         
        public static string GetTypedValueAsString(object typedValue)
        {
            string value = string.Empty;

            if (typedValue.GetType() == typeof(OptionSetValue))
            {
                value = ((OptionSetValue)typedValue).Value.ToString();
            }
            else if (typedValue.GetType() == typeof(EntityReference))
            {
                var _ref = (EntityReference)typedValue;
                value = $"LogicalName:{_ref.LogicalName},Id:{_ref.Id},Name:{_ref.Name}";
            }
            else
            {
                value = typedValue.ToString();
            } 

            return value;
        }

        public static List<string> GetAuditEnabledEntity()
        {
            List<string> _entity = new List<string>();
            if (string.IsNullOrEmpty(Helpers.GetKeyFromAppConfig("Connect_DB"))
                && string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("privateKeyFileLocation")))
            {
                throw new Exception("Connect_DB connection string is empty");
            }

            var connectionString = Helpers.GetKeyFromAppConfig("Connect_DB");

            var privateKeyFilePath = ConfigurationManager.AppSettings.Get("privateKeyFileLocation");
            
            var privateKey = File.ReadAllText(privateKeyFilePath);
            var connDecrypted = Decryptor.Decrypt(connectionString, privateKey);

            string queryString =
            "SELECT LogicalName " +
            "FROM MetadataSchema.Entity " +
            "WHERE IsAuditEnabled = 1 AND OriginalLocalizedName IS NOT NULL " +
            "AND LogicalName NOT IN ('activitymimeattachment', 'systemuser', 'team', 'email'," +
                                    "'contact', 'mli_contactmethod', 'mli_policyaccountrole') " +
            "ORDER BY NAME";

            _entity.Clear();
            using (SqlConnection connection = new SqlConnection(connDecrypted))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        _entity.Add(reader["LogicalName"].ToString());
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Logger.Info(ex.Message);
                }
            }

            return _entity;
        }
    }
}
