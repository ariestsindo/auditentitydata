﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace MLI.CRM.AUDITLOG.AUDITENTITYDATA.CONSOLEAPP
{
    public static class SetFetchPagingQuery
    {
        public static void SetPagingQuery(IOrganizationService service, 
            int fetchCount, StringBuilder fetch, out EntityCollection result, int pageCount,
            out bool isMoreRecordExist)
        {
            //int pageNumber = 1;
            string pagingCookie = null;
            //EntityCollection results = new EntityCollection();

            //while (true)
            //{
                // Build fetchXml string with the placeholders.
                string xml = CreateXml(fetch.ToString(), pagingCookie, pageCount, fetchCount);

                // Excute the fetch query and get the xml result.
                RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
                {
                    Query = new FetchExpression(xml)
                };

                result = service.RetrieveMultiple(fetchRequest1.Query);

                //foreach (Entity list in result.Entities)
                //{
                //    results.Entities.Add(list);
                //}

                // Check for morerecords, if it returns 1.
                if (result.MoreRecords)
                {
                // Increment the page number to retrieve the next page.
                    isMoreRecordExist = true;
                    pagingCookie = result.PagingCookie;
                }
                else
                {
                    isMoreRecordExist = false;
                    // If no more records in the result nodes, exit the loop.
                    //break;
                }
            //}
            //return results;
        }

        public static EntityCollection SetPagingQuery(IOrganizationService service, int fetchCount, QueryExpression fetch)
        {
            int pageNumber = 1;
            fetch.PageInfo.PagingCookie = null;
            EntityCollection results = new EntityCollection();

            while (true)
            {
                fetch.PageInfo.PageNumber = pageNumber;
                fetch.PageInfo.Count = fetchCount;
                EntityCollection result = service.RetrieveMultiple(fetch);

                foreach (Entity list in result.Entities)
                {
                    results.Entities.Add(list);
                }

                // Check for morerecords, if it returns 1.
                if (result.MoreRecords)
                {
                    // Increment the page number to retrieve the next page.
                    pageNumber++;
                    fetch.PageInfo.PagingCookie = result.PagingCookie;
                }
                else
                {
                    // If no more records in the result nodes, exit the loop.
                    break;
                }
            }

            return results;
        }
        
        private static string CreateXml(string xml, string cookie, int page, int count)
        {
            StringReader stringReader = new StringReader(xml);
            XmlTextReader reader = new XmlTextReader(stringReader);

            // Load document
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            return CreateXml(doc, cookie, page, count);
        }

        private static string CreateXml(XmlDocument doc, string cookie, int page, int count)
        {
            XmlAttributeCollection attrs = doc.DocumentElement.Attributes;

            if (cookie != null)
            {
                XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
                pagingAttr.Value = cookie;
                attrs.Append(pagingAttr);
            }

            XmlAttribute pageAttr = doc.CreateAttribute("page");
            pageAttr.Value = System.Convert.ToString(page);
            attrs.Append(pageAttr);

            XmlAttribute countAttr = doc.CreateAttribute("count");
            countAttr.Value = System.Convert.ToString(count);
            attrs.Append(countAttr);

            StringBuilder sb = new StringBuilder(1024);
            StringWriter stringWriter = new StringWriter(sb);

            XmlTextWriter writer = new XmlTextWriter(stringWriter);
            doc.WriteTo(writer);
            writer.Close();

            return sb.ToString();
        }
    }
}